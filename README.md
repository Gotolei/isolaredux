# Isola Verde Redux

### Base track
- [ ] Cam nodes
- [ ] GFX image
- [ ] New readme

### Gameplay
- [x] Widen white slide at the end of first sector
- [x] No camera flag on bottom of ramp leading up to large blue slide
- [x] Fullwidth the blue slide stairs
- [ ] More visual direction for where to go
    - towels draped over railings
    - change in ground tile pattern to match path
        - see market tiles, green accent
        - 45-deg rotation?
    - rope/chain path dividers (see end of muse2)
    - floating lane dividers along surface of pool
    - lanes painted along bottom of pools
    - things that a water park would have on carts, random stacks, etc
        - life vests
        - inner tubes
        - pool noodles?
        - kick boards
        - overturned chairs
        - barriers
- [ ] Method to exit pools other than choke points
    - ramp(s)?
    - force field bubble columns? (instances can have texanim since 20.1230a)
- [ ] varied height inside pools like a real pool
    - painted lanes at pool bottom?
- [ ] lower underwater force field intensity? convert to acceleration?

### Flair
- [x] Split off chairs into separate meshes so negative lighting works on them
    - chairs.prm, umbrella.prm
- [ ] Pair sparkgens with visiboxes so we can have more than eight particles at a time
    - re-evaluate sparkgens before visiboxing them up. test how many particles each generates, and how many can be active at a time without messing with fireworks/shockwaves/fuses etc
- [ ] Ambient sounds. people?, water, breeze, buzzing lights under big slide
- [ ] Drain/mesh transition texture around sides of pools and sloped exits
- [ ] Materials
    - driving on slides creates water particles
        - slightly more slippery? make white slide less deadly in grippy cars
    - can we have a lightly reflective "wet" one without waterboxes fking dying
    - driving underwater creates bubbles?
- [ ] Texanim water flowing down slides
- [ ] Texanim water effect at bottom of pools?
    - surface??
- [ ] Put the anime glasses in the gift shop

## Notes
[Discord chitchat](https://discord.com/channels/85338251597983744/469919290338836483/805123948499959860)
- Permission granted from original author crescviper: credit given in new readme. Planning on including original readme as well
- Brainstorming for un-petrovolting race line


